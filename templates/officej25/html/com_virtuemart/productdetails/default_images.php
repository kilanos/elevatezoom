`<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Valerie Isaksen

 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_images.php 6188 2012-06-29 09:38:30Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
vmJsApi::js( 'fancybox/jquery.fancybox-1.3.4.pack');
vmJsApi::css('jquery.fancybox-1.3.4');
$document = JFactory::getDocument ();
$imageJS = '
jQuery(document).ready(function() {

	jQuery("a[rel=vm-additional-images]").fancybox({
		"titlePosition" 	: "inside",
		"transitionIn"	:	"elastic",
		"transitionOut"	:	"elastic"
	});
	
	jQuery(".additional-images .product-image").click(function() {
		jQuery(".main-image img").attr("src",this.src );
		jQuery(".main-image img").attr("alt",this.alt );
		jQuery(".main-image a").attr("href",this.src );
		jQuery(".main-image a").attr("title",this.alt );
	}); 
	
	//declare jQuery elevateZoom 3.0.8, for more info: http://www.elevateweb.co.uk/image-zoom/configuration
	jQuery("#zoom_03").elevateZoom({gallery:\'gallery_01\', cursor: \'pointer\', galleryActiveClass: \'active\', imageCrossfade: true, zoomWindowOffetx: -600, zoomWindowHeight: 450,loadingIcon: \'http://www.elevateweb.co.uk/spinner.gif\'});

	jQuery("#zoom_03").bind("click", function(e) {  
		var ez =   jQuery(\'#zoom_03\').data(\'elevateZoom\');	
		jQuery.fancybox(ez.getGalleryList());
		return false;
	});
	
});';

//call the script of elevateZoom 
$document->addScript('/furniture/components/com_virtuemart/assets/js/jquery.elevatezoom.js');
$document->addScriptDeclaration ($imageJS);

echo '<pre>';
	//print_r($this->product->images);
echo '</pre>';
//echo $this->product->images[0]->file_url;
if (!empty($this->product->images)) {
	$image = $this->product->images[0];
	?>
<div class="main-image">

	<?php	
	
		//echo preg_replace("/<a/", "<a id=\"zoom01\" class=\"cloud-zoom\" rel=\"position:'left', adjustX:-20, adjustY:-3, tint:'none;', softFocus:0, smoothMove:3, tintOpacity:0.8\" ", $image->displayMediaFull(" ",true,"rel='vm-additional-images'"));		
		//echo $image->displayMediaFull("",true,"rel='vm-additional-images'");		
		echo preg_replace("/<img[^>]+\>/i", " <img id=\"zoom_03\" src=\"".$this->product->images[0]->file_url_thumb."\" data-zoom-image=\"".$this->product->images[0]->file_url."\"/> ", $image->displayMediaFull(" ",true,"rel='vm-additional-images'"));		
		
	?>

	 <div class="clear"></div>
</div>
<?php
	$count_images = count ($this->product->images);
	if ($count_images > 1) {
		?>
    <div id="gallery_01" class="additional-images">
		<?php
		for ($i = 1; $i < $count_images; $i++) {
			$image = $this->product->images[$i];
			?>
            <div class="floatleft">
	            <?php					
	                //echo $image->displayMediaFull("",true,"rel='vm-additional-images'");
					//echo $image->displayMediaFull('class="product-image" style="cursor: pointer"',false,"");										
					echo preg_replace("/<img[^>]+\>/i", " <a href=\"#\" data-image=\"".$this->product->images[$i]->file_url_thumb."\" data-zoom-image=\"".$this->product->images[$i]->file_url."\"> <img id=\"img_01\" src=\"".$this->product->images[$i]->file_url_thumb."\" /> </a> ", $image->displayMediaFull(" ",true,"rel='vm-additional-images'"));		
	            ?>
            </div>
			<?php
		}
		?>
        <div class="clear"></div>
    </div>
	<?php
	}
}
  // Showing The Additional Images END 
?>